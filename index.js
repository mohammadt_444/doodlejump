const mainFrame = (function mainFrameCreator() {
    let mainFrame = document.createElement('div')
    mainFrame.style.backgroundImage = "url(assets/hop-bck@2x.png)"
    mainFrame.style.backgroundSize = "cover"
    mainFrame.style.height = "100vh"
    mainFrame.style.width = "100vw"
    mainFrame.style.justifyContent = "center"
    mainFrame.style.justifyItems = "center"
    mainFrame.style.alignContent = "center"
    mainFrame.style.alignItems = "center"
    document.body.appendChild(mainFrame);
    return mainFrame
})();
const gameFrame = (function gameFrameCreator() {
    let gameFrame = document.createElement('div')
    gameFrame.style.position = "absolute"
    gameFrame.style.backgroundImage = "url(assets/bck@2x.png)"
    gameFrame.style.left = "30%"
    gameFrame.style.width = "40%"
    gameFrame.style.height = "100%"
    gameFrame.style.justifyContent = "center"
    gameFrame.style.alignItems = "center"
    gameFrame.style.float = "center"
    gameFrame.style.alignContent = "center"
    gameFrame.style.overflow = "hidden"
    mainFrame.appendChild(gameFrame)
    return gameFrame
})();

(function startMenuCreator(){
    let playButton = document.createElement('img')
    playButton.src = "assets/play@2x.png"
    playButton.style.position = "absolute"
    playButton.style.top = "50%"
    playButton.style.left = "30%"
    playButton.addEventListener("click", () => {
        gameFrame.innerHTML = ''
        handler.gameHandler()
    })
    let doodleJumpElement = document.createElement("img")
    doodleJumpElement.src = "assets/doodleJump.png"
    doodleJumpElement.style.position = "absolute"
    doodleJumpElement.style.top = "30%"
    doodleJumpElement.style.left = "25%"
    doodleJumpElement.style.overflow = "hidden"
    doodleJumpElement.style.zIndex = "2"


    gameFrame.appendChild(doodleJumpElement)
    gameFrame.appendChild(playButton)
})();

const handler = (function Handlers(){
    const soundCreator = (function soundCreatorFunc(){
        let jumpSound = document.createElement("audio")
        let loseSound = document.createElement("audio")
        let federSound = document.createElement("audio")
        jumpSound.src = "assets/jump.ogg"
        loseSound.src = "assets/lose.ogg"
        federSound.src = "assets/feder.ogg"
        federSound.load()
        loseSound.load()
        jumpSound.load()
        return {
            jump: () => jumpSound.play(),
            lose: () => loseSound.play(),
            feder: () => federSound.play()
        }
    })();
    function gameHandler() {
        function federActivator(feder){
            feder.src = "assets/federOn.png"
            feder.style.top = R.compose(
                (x) => x + "%",
                R.add(-1.5),
                (x) => Number(x),
                R.slice(0, -1)
            )(feder.style.top)
        }
        function wallCreator(yLoc) {
            function federCreator(yloc, leftLoc){
                if (Math.random() > 0.9) {
                    let feder = document.createElement("img")
                    feder.className = "feder"
                    feder.src = "assets/feder.png"
                    feder.style.position = "absolute"
                    feder.style.top = yloc - 1 + "%"
                    feder.style.left = leftLoc + 6 + "%"
                    feder.style.overflow = "hidden"
                    feder.style.zIndex = 1
                    gameFrame.appendChild(feder)
                    console.log("feder")
                }
            }
            let wall = document.createElement("img")
            let leftLoc = Math.floor(Math.random() * 85)
            yLoc = yLoc || -10
            wall.className = "wall"
            wall.src = "assets/wall.png"
            wall.style.position = "absolute"
            wall.style.top = yLoc + "%"
            wall.style.left = leftLoc + "%"
            wall.style.overflow = "hidden"
            wall.style.zIndex = 1
            federCreator(yLoc, leftLoc)
            return wall
        }
        function doodleJumpCreator() {
            let doodlerElement = document.createElement("img")
            doodlerElement.className = "doodler"
            doodlerElement.src = "assets/doodler.png"
            doodlerElement.style.position = "absolute"
            doodlerElement.style.top = "75%"
            doodlerElement.style.left = "50%"
            doodlerElement.style.overflow = "hidden"
            doodlerElement.style.zIndex = "2"
            doodlerElement.movingRight = true
            return doodlerElement
        }
        function scoreElementMaker() {
            let scoreEle = document.createElement("p")
            scoreEle.style.position = "absolute"
            scoreEle.style.top = "0%"
            scoreEle.style.left = "5%"
            scoreEle.style.zIndex = "3"
            scoreEle.style.fontSize = "300%"
            scoreEle.style.fontFamily = "doodleJump"
            return scoreEle
        }
        function detection(walls,feders) {
            function overlap(element){
                let doodleLeft
                let doodleRight
                if(doodlerElement.movingRight === true){
                    doodleRight = doodlerElement.x + Math.floor(doodlerElement.width*2/3)
                    doodleLeft = doodlerElement.x
                }
                else{
                    doodleRight = doodlerElement.x + doodlerElement.width
                    doodleLeft = doodlerElement.x + Math.floor(doodlerElement.width/3)
                }
                let doodleTop = doodlerElement.y + doodlerElement.height - 1
                let doodleBottom = doodlerElement.y + doodlerElement.height

                let wallLeft = element.x
                let wallTop = element.y
                let wallRight = element.x + element.width
                let wallBottom = element.y + element.height
                return !(wallRight < doodleLeft ||
                    wallLeft > doodleRight ||
                    wallBottom < doodleTop ||
                    wallTop > doodleBottom)
            }
            if (YmovementScale > 0) {
                feders.forEach((feder) => {
                    if (overlap(feder)) {
                        federActivator(feder)
                        soundCreator.feder()
                        YmovementScale = -1.5
                    }
                })

                Array.from(walls).forEach((wall) => {
                    if (overlap(wall)) {
                        soundCreator.jump()
                        YmovementScale = -0.7
                    }
                })
            }
        }
        let moveRight = false
        let moveLeft = false
        let YmovementScale = -0.6
        let XmovementScale = 0
        let score = 0
        let lastScoreWallMake = -10
        const doodlerElement = doodleJumpCreator();
        const scoreElement = scoreElementMaker();

        (function keyListenerAdder(){
            document.addEventListener("keydown", (e) => {
                console.log(e.key)
                if (e.key === "ArrowRight") {
                    moveRight = true
                }
                if (e.key === "ArrowLeft") {
                    moveLeft = true
                }
            });
        })();
        (function baseWallsCreator() {
            for (let i = 10; i <= 90; i += 10) {
                let wall = wallCreator(i)
                gameFrame.appendChild(wall)
            }
        })();

        gameFrame.appendChild(doodlerElement)
        gameFrame.appendChild(scoreElement)

        let gameHandlerID = setInterval(() => {
            let walls = document.getElementsByClassName("wall")
            let feders = Array.from(document.getElementsByClassName("feder"))
            let difference = 0

            let doodlerLocation = R.compose(
                R.add(YmovementScale),
                (x) => Number(x),
                R.slice(0, -1)
            )(doodlerElement.style.top)
            YmovementScale += 0.01
            if (doodlerLocation < 30) {
                difference = 30 - doodlerLocation
                doodlerLocation = 30
            }
            doodlerElement.style.top = doodlerLocation + "%"
            if (doodlerLocation > 100) {
                clearInterval(gameHandlerID)
                loseHandler(score)
            }
            if (moveRight === true) {
                XmovementScale += 1
                doodlerElement.src = "assets/doodler.png"
                doodlerElement.movingRight = true
                moveRight = false
            }
            if (moveLeft === true) {
                XmovementScale -= 1
                doodlerElement.src = "assets/doodlerLeft.png"
                doodlerElement.movingRight = false
                moveLeft = false
            }
            if(XmovementScale > 0.1 || XmovementScale < -0.1){
                let doodlerXloc = R.compose(
                    (x) => Number(x),
                    R.slice(0, -1)
                )(doodlerElement.style.left)
                doodlerXloc += XmovementScale
                if (XmovementScale > 0) {
                    XmovementScale += -0.04
                }
                else {
                    XmovementScale += 0.05
                }
                if (doodlerXloc < -5) {
                    doodlerXloc += 100
                }
                if (doodlerXloc > 95){
                    doodlerXloc -= 100
                }
                doodlerElement.style.left = doodlerXloc + "%"
            }
            score += difference
            if (score > lastScoreWallMake + 10){
                gameFrame.appendChild(wallCreator())
                lastScoreWallMake = score
            }
            if (difference) {
                Array.from(walls).forEach(
                    (x) => {
                        x.style.top = R.compose(
                            (x) => x + "%",
                            R.add(difference),
                            (x) => Number(x),
                            R.slice(0, -1)
                        )(x.style.top)
                    }
                )

                feders.forEach(
                    (x) => {
                        x.style.top = R.compose(
                            (x) => x + "%",
                            R.add(difference),
                            (x) => Number(x),
                            R.slice(0, -1)
                        )(x.style.top)
                    }
                )

                feders.forEach(
                    (x) => {
                        let top = R.compose(
                            (x) => Number(x),
                            R.slice(0, -1)
                        )(x.style.top)

                        if (top > 100) {
                            x.remove()
                        }
                    }
                )

                Array.from(walls).forEach(
                    (x) => {
                        let top = R.compose(
                            (x) => Number(x),
                            R.slice(0, -1)
                        )(x.style.top)

                        if (top > 100) {
                            x.remove()
                        }
                    }
                )
            }
            detection(walls,feders)
            scoreElement.innerHTML = "score : " + Math.floor(score*10)
        }, 10)
    }
    function loseHandler(score){
        soundCreator.lose()
        let Elements = Array.from(document.getElementsByClassName("wall"));
        Array.from(document.getElementsByClassName("feder")).forEach((x) => Elements.push(x));

        (function gameOverElementCreator() {
            let gameOverElement = document.createElement("img")
            // gameOverElement.className = "doodler"
            gameOverElement.src = "assets/gameOver.png"
            gameOverElement.style.position = "absolute"
            gameOverElement.style.top = "280%"
            gameOverElement.style.left = "25%"
            gameOverElement.style.overflow = "hidden"
            gameOverElement.style.zIndex = "2"
            gameFrame.appendChild(gameOverElement)
            Elements.push(gameOverElement)
        })();

        (function scoreElementCreator() {
            let scoreElement = document.createElement("p")
            scoreElement.style.position = "absolute"
            scoreElement.style.top = "310%"
            scoreElement.style.left = "30%"
            scoreElement.style.zIndex = "2"
            scoreElement.style.fontSize = "400%"
            scoreElement.style.fontFamily = "doodleJump"
            scoreElement.innerHTML = "your Score : " + Math.floor(score*10)
            gameFrame.appendChild(scoreElement)
            Elements.push(scoreElement)
        })();

        (function playAgainElementCreator() {
            let playAgainElement = document.createElement("img")
            playAgainElement.src = "assets/playAgain.png"
            playAgainElement.style.position = "absolute"
            playAgainElement.style.top = "330%"
            playAgainElement.style.left = "40%"
            playAgainElement.style.overflow = "hidden"
            playAgainElement.style.zIndex = "2"
            playAgainElement.addEventListener("click",()=>{
                gameFrame.innerHTML = ''
                gameHandler()
            })
            gameFrame.appendChild(playAgainElement)
            Elements.push(playAgainElement)
        })();

        let counter = 0
        let loseHandlerID = setInterval(()=>{
            Elements.forEach(
                (x) => {
                    x.style.top = R.compose(
                        (x) => x + "%",
                        R.add(-2.5),
                        (x) => Number(x),
                        R.slice(0, -1)
                    )(x.style.top)
                }
            )
            counter += 2.5
            if(counter === 260){
                console.log("lost")
                clearInterval(loseHandlerID)
            }
        },10)

    }
    return {
        gameHandler,
        loseHandler
    }
})();

